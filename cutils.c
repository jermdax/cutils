#include "cutils.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

char* ftostr(char* fpath)
{
	unsigned int nread = 0;
	const unsigned int max_len = 4096;
	int fd = open(fpath, O_RDONLY);
	if(fd == -1)
		return NULL;
	char *dest = (char*)malloc(max_len);
	nread = read(fd, dest, max_len);
	dest[nread] = '\0';
	return dest;
}

int is_ascii_num(char c)
{
	return c >= '0' && c <= '9';
}

int is_whitespace(char c)
{
	return  c == ' ' || c == '\t' || c == '\n';
}
