all:
	gcc cutils.c -c -fpic -o cutils.o -g
	gcc -shared -o libcutils.so cutils.o
clean:
	rm cutils.o libcutils.so
