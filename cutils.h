#ifndef CUTILS_H
#define CUTILS_H

char* ftostr(char* fpath);
int is_ascii_num(char c);
int is_whitespace(char c);

#endif
